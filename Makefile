
hash: main.c
	#TODO: Compilación con linking estático
hash: main.c lib/libsha1.a 
	gcc -c main.c -Iinclude
	gcc -static -o hash main.o lib/libsha1.a
lib/libsha1.a: 
	ar rcs lib/libsha1.a 

hash_dyn: lib/libsha1.so.0.0.0 main.c
	gcc -o hash_dyn main.c -Iinclude lib/libsha1.so.0.0.0

lib/libsha1.so.0.0.0:
	gcc -shared -fPIC -o lib/libsha1.so.0.0.0

.PHONY: clean
clean:
	rm -f hash *.o 
	rm -f hash_dyn *.o